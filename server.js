var app = require('express')()
var http = require('http').Server(app)
var io = require('socket.io')(http)

io.on('connection', function(socket) {
  console.log('New connection')

  socket.on('media-changed', function(route) {
    console.log('Route changed', route)

    socket.broadcast.emit('media-changed', route)
  })
})

app.get('/force-client-update', function (req, res) {
	io.sockets.emit('force-update');
  	res.send('OK')
})

http.listen(6001, function() {
  console.log('listening on *:6001')
})
